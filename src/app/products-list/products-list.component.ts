import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Product } from '../models/product.model';
import { Store } from '@ngrx/store';
import { ProductsState } from '../store/reducers/products.reducer';
import { getAllItems, getColumns } from '../store/products.selectors';
import { LoadProducts } from '../store/actions/products.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products-list',
  template: `<div class="products table-responsive">
    <ng-container *ngIf="tableColumns$ | async as columns">
      <table class="table">
      <thead class="thead-dark">
          <tr>
            <th scope="col" *ngFor="let column of columns">{{column}}</th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="let product of (products$ | async)">
            <td *ngFor="let column of columns">
              {{product[column]}}
            </td>
          </tr>
        </tbody>

      </table>
    </ng-container>

  </div>
  `,
  styleUrls: ['./products-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsListComponent implements OnInit {
  products$: Observable<Product[]>;
  tableColumns$: Observable<string[]>;
  constructor(private store: Store<ProductsState>) {}

  ngOnInit() {
    this.store.dispatch(new LoadProducts());
    this.products$ = this.store.select(getAllItems);
    this.tableColumns$ = this.store.select(getColumns);
  }
}
