import { Injectable } from '@angular/core';

import { Effect, Actions } from '@ngrx/effects';

import {
  ProductsActionTypes,
  LoadProductsSuccess,
  LoadProductsFail
} from '../actions/products.actions';
import { ProductsService } from '../../products.service';
import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ProductsEffects {
  constructor(private actions$: Actions, private productsService: ProductsService) {}
  @Effect()
  loadProducts$ = this.actions$.ofType(ProductsActionTypes.Load).pipe(
    switchMap(() => {
      return this.productsService.getProducts().pipe(
        map(products => new LoadProductsSuccess(products)),
        catchError(error => of(new LoadProductsFail(error)))
      );
    })
  );
}
