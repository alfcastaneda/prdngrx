import { createSelector } from '@ngrx/store';

import * as fromProducts from './reducers/';
import { getProductsEntities, getProductsProperties } from './reducers/products.reducer';

export const getProductsState: any = createSelector(fromProducts.getProductsState);

export const getColumns = createSelector(getProductsState, getProductsProperties);
export const getProducts = createSelector(getProductsState, getProductsEntities);

export const getAllItems = createSelector(getProducts, products =>
  Object.keys(products).map(id => ({
    ...products[parseInt(id, 10)]
  }))
);
