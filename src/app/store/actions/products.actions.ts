import { Action } from '@ngrx/store';

import { Product } from '../../models/product.model';
import { Filter } from '../reducers/products.reducer';

export enum ProductsActionTypes {
  Load = '[Products] Load programs',
  LoadSuccess = '[Products] Load complete',
  LoadFail = '[Products] Load products failure',
  Filter = '[Products] Filter products'
}

export class LoadProducts implements Action {
  readonly type = ProductsActionTypes.Load;
}

export class LoadProductsSuccess implements Action {
  readonly type = ProductsActionTypes.LoadSuccess;
  constructor(public payload: Product[]) {}
}

export class LoadProductsFail implements Action {
  readonly type = ProductsActionTypes.LoadFail;
  constructor(public payload: any[]) {}
}

export class FilterProducts implements Action {
  readonly type = ProductsActionTypes.Filter;
  constructor(public payload: { [column: string]: Filter }) {}
}

// action types
export type ProductsActionsUnion =
  | LoadProducts
  | LoadProductsSuccess
  | LoadProductsFail
  | FilterProducts;
