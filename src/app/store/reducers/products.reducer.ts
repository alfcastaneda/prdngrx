import { ProductsActionsUnion, ProductsActionTypes } from '../actions/products.actions';
import { Product } from '../../models/product.model';

export interface Filter {
  column: string;
  operator: string;
  value: any;
  type: string;
}

export interface ProductsState {
  entities: { [id: number]: Product };
  columns: string[];
  filters: Filter[];
  loaded: boolean;
  loading: boolean;
}

export const INITIAL_STATE: ProductsState = {
  entities: {},
  columns: [],
  loading: false,
  loaded: false,
  filters: []
};

function getMatchedStringEntities(
  entities: { [id: number]: Product },
  filter: Filter
): { [id: number]: Product } {
  const keys = Object.keys(entities);
  const newEntities = keys.reduce((acc, key) => {
    let value;
    if (filter.operator === 'contains') {
      value = entities[key][filter.column].toLowerCase().includes(filter.value)
        ? entities[key]
        : null;
    } else {
      value = !entities[key][filter.column].toLowerCase().includes(filter.value)
        ? entities[key]
        : null;
    }
    if (value) {
      acc[key] = value;
    }
    return acc;
  }, {});
  return newEntities;
}

function getMatchedNumberEntities(
  entities: { [id: number]: Product },
  filter: Filter
): { [id: number]: Product } {
  const keys = Object.keys(entities);
  const newEntities = keys.reduce((acc, key) => {
    let value;
    const numValue = parseFloat(filter.value).toFixed(2);
    switch (filter.operator) {
      case '<': {
        value = entities[key][filter.column] < numValue ? entities[key] : null;
        break;
      }
      case '<=': {
        value = entities[key][filter.column] <= numValue ? entities[key] : null;
        break;
      }
      case '>': {
        value = entities[key][filter.column] > numValue ? entities[key] : null;
        break;
      }
      case '>=': {
        value = entities[key][filter.column] >= numValue ? entities[key] : null;
        break;
      }
      case '==': {
        value = entities[key][filter.column] === numValue ? entities[key] : null;
        break;
      }
      case '!=': {
        value = entities[key][filter.column] !== numValue ? entities[key] : null;
        break;
      }
    }
    if (value) {
      acc[key] = value;
    }
    return acc;
  }, {});
  console.log(newEntities);
  return newEntities;
}

export function reducer(
  state = INITIAL_STATE,
  action: ProductsActionsUnion
): ProductsState {
  switch (action.type) {
    case ProductsActionTypes.Load: {
      return { ...state, loading: true };
    }
    case ProductsActionTypes.LoadFail: {
      return { ...state, loaded: false, loading: false };
    }
    case ProductsActionTypes.LoadSuccess: {
      const entities = action.payload;
      const columns = Object.keys(entities[Object.keys(entities)[0]]);
      return { ...state, loading: false, loaded: false, entities, columns };
    }
    case ProductsActionTypes.Filter: {
      const filters = action.payload;
      const filtersKeys = Object.keys(filters);
      let newEntities = Object.assign({}, state.entities);
      filtersKeys.forEach(element => {
        if (filters[element].type === 'string') {
          newEntities = getMatchedStringEntities(newEntities, filters[element]);
        } else {
          newEntities = getMatchedNumberEntities(newEntities, filters[element]);
        }
      });
      return Object.assign({}, state, { entities: newEntities });
    }
    default: {
      return state;
    }
  }
}

export const getProductsLoading = (state: ProductsState) => state.loading;
export const getProductsLoaded = (state: ProductsState) => state.loaded;
export const getProductsEntities = (state: ProductsState) => state.entities;
export const getProductsProperties = (state: ProductsState) => state.columns;
