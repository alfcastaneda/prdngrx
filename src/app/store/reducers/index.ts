import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as ProductsReducerModule from './products.reducer';

export interface ProductsState {
  products: ProductsReducerModule.ProductsState;
}

export const reducers: ActionReducerMap<ProductsState> = {
  products: ProductsReducerModule.reducer
};

export const getProductsState = createFeatureSelector<ProductsState>('products');
