import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Product } from './models/product.model';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product[]> {
    return this.http
      .get<Product[]>('api/products')
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}
