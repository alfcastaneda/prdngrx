import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Product } from '../models/product.model';
import { Store } from '@ngrx/store';
import { ProductsState } from '../store/reducers/products.reducer';
import { Observable } from 'rxjs';
import { Filter } from '../store/reducers/products.reducer';
import { FilterProducts } from '../store/actions/products.actions';
import { getColumns } from '../store/products.selectors';

@Component({
  selector: 'app-products-filters',
  template: `
    <div class="nav-item">
      <div class="filter-btn">
        <button type="button" (click)="addFilter()" class="btn btn-primary">+ Filter</button>
      </div>
      <div class="filters-container">
        <div *ngIf="filtersCollection.length > 0">
            <app-single-filter
              *ngFor="let filter of filtersCollection"
              [fieldOptions]="tableColumns$ | async"
              (filled)="onValidFilter($event)"></app-single-filter>
        </div>
      </div>

      <button type="button" [disabled]="!filtersCollection.length"
        (click)="applyFilters()" class="btn btn-secondary btn-lg">Apply Filters</button>
    </div>
  `,
  styleUrls: ['./products-filters.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsFiltersComponent implements OnInit {
  filters: { [property: string]: Filter } = {};
  filtersCollection: any = [];
  products$: Observable<Product[]>;
  tableColumns$: Observable<string[]>;
  constructor(private store: Store<ProductsState>) {}

  ngOnInit() {
    this.tableColumns$ = this.store.select(getColumns);
  }
  addFilter() {
    this.filtersCollection.push({ index: this.filtersCollection.length - 1 });
  }

  onValidFilter(value: Filter) {
    this.filters[value.column] = value;
    this.filters[value.column].value = this.filters[value.column].value
      .toString()
      .toLowerCase();
  }

  applyFilters() {
    this.store.dispatch(new FilterProducts(this.filters));
  }
}
