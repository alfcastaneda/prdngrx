import {
  Component,
  Output,
  EventEmitter,
  OnChanges,
  ChangeDetectionStrategy,
  Input,
  SimpleChanges
} from '@angular/core';

import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators
} from '@angular/forms';

import { Filter } from '../../store/reducers/products.reducer';
import { isNumber } from 'util';

@Component({
  selector: 'app-single-filter',
  template: `
    <form [formGroup]="form">
      <div class="form-group">
        <label for="filter_type">Filter type</label>
        <select formControlName="column" id="filter_type" (change)="typeSelected()" class="form-control form-control-lg">
          <ng-container *ngFor="let option of fieldOptions">
            <option *ngIf="option != 'id'" [value]="option">{{option | titlecase}}</option>
          </ng-container>
        </select>
      </div>
      <ng-container  *ngIf="hasFieldSelected" >
        <div class="form-group">
            <select formControlName="operator" *ngIf="checkFieldType() else stringTypeBlock"
              (change)="enableTextInput()" class="form-control form-control-lg">
              <option *ngFor="let operator of numberOptions" [value]="operator">{{operator}}</option>
            </select>
            <ng-template #stringTypeBlock>
              <select formControlName="operator" (change)="enableTextInput()" class="form-control form-control-lg">
                <option *ngFor="let operator of stringOptions" [value]="operator">{{operator}}</option>
              </select>
            </ng-template>
          </div>
          <div class="form-group">
            <input formControlName="value"
              class="form-control form-control-lg"
              placeholder=""
              type="{{checkFieldType() ? 'number' : 'text'}}"
              (keyup)="emitChange()"/>
          </div>
      </ng-container>
    </form>
  `,
  styleUrls: ['./single-filter.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleFilterComponent implements OnChanges {
  @Input()
  fieldOptions: any;
  @Output()
  filled = new EventEmitter<Filter>();

  hasFieldSelected: boolean;
  NUMBER_FILTERS = ['gtin', 'price', 'quantity'];
  numberOptions = ['<', '<=', '>', '>=', '==', '!='];
  stringOptions = ['contains', 'does_not_contain'];
  form = this.fb.group({
    column: ['', Validators.required],
    operator: ['', Validators.required],
    value: [{ value: '', disabled: true }, Validators.required],
    type: [{ value: '' }]
  });
  constructor(private fb: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges) {}

  typeSelected() {
    this.form.get('value').reset();
    this.hasFieldSelected = this.form.get('column').value !== '';
  }

  checkFieldType() {
    const isNumberInput = this.NUMBER_FILTERS.includes(this.form.get('column').value);
    this.form
      .get('value')
      .setValidators(
        isNumberInput
          ? Validators.required
          : [Validators.required, Validators.minLength(3)]
      );
    this.form.get('type').setValue(isNumberInput ? 'number' : 'string');
    return isNumberInput;
  }

  getInputType() {
    return this.checkFieldType() ? 'number' : 'text';
  }

  enableTextInput() {
    if (this.form.get('operator').valid) {
      this.form.get('value').enable();
    } else {
      this.form.get('value').disable();
    }

    if (this.form.valid) {
      this.filled.emit(this.form.value);
    }
  }

  emitChange() {
    const { value, valid } = this.form;
    if (valid) {
      this.filled.emit(value);
    }
  }
}
