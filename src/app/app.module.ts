import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { reducers } from './store/reducers';
import { effects } from './store/effects';
import { ProductsFiltersComponent } from './products-filters/products-filters.component';
import { SingleFilterComponent } from './products-filters/single-filter/single-filter.component';
@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    ProductsFiltersComponent,
    SingleFilterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
